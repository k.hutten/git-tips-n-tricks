# Basic overview and guide for git and best repo maintenance

## Intro

Git is a powerful tool for version control, though it can be a little like 'going down the rabbit-hole' in terms of complexity. Keeping to the basic features will give us 90% of the benefits. 

Somewhat aside to the features of git are best practices of maintaining a code repo. This is worth covering because in some ways the use of git/version control is itself a best practice, and getting this right means having a really solid code base that everyone feels at home using (and using all parts of it not just the part they have written) increasing productivity and possibly more importantly reducing frustration. To condense best practices to three things they might be:

- Writing good code (modular, sef-documenting etc)
- Writing good commit messages
- Having a code review process.

Writing good code is a massive topic and something that even senior developers aim to keep improving, but luckily having a good code review process should be relatively easy to get right straight away and will help improve the former for everybody over time.

## git quick reference

Hopefully when after quick answers the following with get you there 90% of the time.

### safe git commands that should be used often
```
git status
git log
git log --oneline
git branch
```

### Standard pipeline for making changes

Make changes and save them
`git status` sanity check

`git diff` with no arguments `diff` will show you diffs between last commit and uncommited-unstaged changes

`git add` add all changed files to staging

`git add <file-name>` to add individual files, multiple separated by spaces works

`git status` sanity check

`git diff --staged` diffs between last commit and staged changes

`git commit` commit changes

`git push` pushes changes up to the remote repo (gitlab)

Other tips. Diffs is definitely one of the command line's weaker areas, a GUI is recommended. If you stage things accidentally `git reset HEAD` will unstage everything, though again GUIs are good for staging.

### Standard pipeline for branching
Changes should always be made on a branch and merged into master through your code review process using a pull request on gitlab

`git checkout master` you will want to branch off master most of the time

`git pull` it's a good idea update your local repo before branching

`git checkout -b branch-name` will make a branch and put you on it, branch-name can't have spaces so `kebab-case` is a good option

`git status` sanity check

Start making changes and committing them, when you are ready push your branch to gitlab and open a pull request for review.

`git branch` will list current branches

`git branch -d branch-name` deletes branch-name, you should be making branches often so knowing how to delete is important.

other tips, you might want merge branches locally (ie not through gitlab pull request)

`git merge branch-name` will merge branch-name into branch you are currently on.

### resolving conflicts

If you try and merge two branches locally and git tells you there is a conflict, it will tell you which files, in those file you will find the syntax
```
>>>>>>>>> HEAD
The line or lines from the target branch (branch you were merging into)
=========
The line or lines from the source branch (the branch you were trying to merge)
<<<<<<<<< name-of-source-branch
```
To resolve the conflict choose which lines you want to keep, or combine both lines in some way that makes sense than remove all of the `>>>>>>>`, `========` and `<<<<<<<` lines.

Stage these changes and make a commit as per usual and ✅ conflict is now resolved.

`git reset --hard HEAD` Will abort the merge, for if you hit a conflict and don't want to deal with it right now.

### Using Gitlab or other repo hosting service for team merging/code 👏 review 👏

Branch as per usual, and make your changes as per usual, when ready . . .

`git push` to get your branch up on the remote repo

Follow the user interface to make a merge request (sometimes called a pull request), you will normally want to aim at master (new branch is source, master is target).

If you need to make any more changes from the code review, make them locally with new commits on the same branch, then push again. The merge request will automatically update.

Merge through the online interface when you and your team is ready.

`git pull` on master to get the freshly merged changes from the remote repo 

it's a good idea to delete the source branch when it's been merged, this will need to be done on your local machine too if you were the one that created the branch.

### Resolving conflicts in a merge request

If you have made a merge request and there is a conflict, gitlab or other hosting service will not let you merge until the conflict has been resolved. Even though we are trying to merge 'new-branch' into master' in our merge request, we can first merge master into our 'new-branch'. This will give us the same conflicts which we can resolve, commit and then push. This will update our merge request and there will be no more conflicts because we dealt with them in the branch we are trying to merge.

i.e.

`git checkout new-branch`

`git merge master`

Resolve conflicts

`git push`

## Good resources 

[Atlassian's excellent git resouces](https://www.atlassian.com/git)

[How to write a good git commit message](https://chris.beams.io/posts/git-commit/)

[Pro git, if you want to be a master](https://git-scm.com/book/en/v2)


## Good git related tools

[VS Code, good editor with git integration](https://code.visualstudio.com/)

[Source tree](https://www.sourcetreeapp.com/)

## Some defaults worth setting up

[first time set up](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)

[SSH keys](https://docs.gitlab.com/ce/ssh/README.html) for username and password-less pushing

[Git push default](https://stackoverflow.com/questions/948354/default-behavior-of-git-push-without-a-branch-specified#948397) behavior

